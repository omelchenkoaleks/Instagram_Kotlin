package com.omelchenkoaleks.instagram.utils

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class CustomGlideMolule : AppGlideModule()