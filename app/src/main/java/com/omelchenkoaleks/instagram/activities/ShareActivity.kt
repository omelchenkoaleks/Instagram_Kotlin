package com.omelchenkoaleks.instagram.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.google.firebase.database.ServerValue
import com.omelchenkoaleks.instagram.R
import com.omelchenkoaleks.instagram.models.User
import com.omelchenkoaleks.instagram.utils.CameraHelper
import com.omelchenkoaleks.instagram.utils.FirebaseHelper
import com.omelchenkoaleks.instagram.utils.GlideApp
import com.omelchenkoaleks.instagram.utils.ValueEventListenerAdapter
import kotlinx.android.synthetic.main.activity_share.*
import org.w3c.dom.Comment
import java.util.*

class ShareActivity : BaseActivity(2) {
    private val TAG = "ShareActivity"

    private lateinit var camera: CameraHelper
    private lateinit var firabase: FirebaseHelper
    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share)
        Log.d(TAG, "onCreate")

        firabase = FirebaseHelper(this)

        camera = CameraHelper(this)
        camera.takeCameraPicture()

        back_image.setOnClickListener { finish() }
        share_text.setOnClickListener { share() }

        // инициализируем юзера
        firabase.currentUserReference().addValueEventListener(ValueEventListenerAdapter {
            user = it.asUser()!!
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == camera.REQUST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                GlideApp.with(this).load(camera.imageUri).centerCrop().into(post_image)
            } else {
                finish()
            }
        }
    }

    private fun share() {
        val imgUri = camera.imageUri
        if (imgUri != null) {
            // upload image to user folder <- storage
            val uid = firabase.auth.currentUser!!.uid
            firabase.storage.child("users").child(uid).child("images")
                .child(imgUri.lastPathSegment).putFile(imgUri).addOnCompleteListener {
                    if (it.isSuccessful) {
                        // add image to user images <- db
                        val imageDownLoadUrl = it.result.downloadUrl!!.toString()
                        firabase.database.child("images").child(uid).push()
                            .setValue(imageDownLoadUrl)
                            .addOnCompleteListener {
                                if (it.isSuccessful) {
                                    firabase.database.child("feed-posts").child(uid)
                                        .push()
                                        .setValue(mkFeedPost(uid, imageDownLoadUrl))
                                        .addOnCompleteListener {
                                            if (it.isSuccessful) {
                                                startActivity(Intent(this,
                                                    ProfileActivity::class.java))
                                                finish()
                                            }
                                        }
                                } else {
                                    showToast(it.exception!!.message!!)
                                }
                            }
                    } else {
                        showToast(it.exception!!.message!!)
                    }
                }
        }
    }

    private fun mkFeedPost(uid: String, imageDownLoadUrl: String): FeedPost {
        return FeedPost(
            uid = uid,
            username = user.username,
            image = imageDownLoadUrl,
            caption = caption_input.text.toString(),
            photo = user.photo!!
        )
    }
}

data class FeedPost(
    val uid: String = "",
    val username: String = "",
    val photo: String = "",
    val likesCount: Int = 0,
    val commentsCount: Int = 0,
    val caption: String = "",
    val comments: List<Comment> = emptyList(),
    val timestamp: Any = ServerValue.TIMESTAMP,
    val image: String? = null
) {
    fun timestampDate(): Date = Date(timestamp as Long)
}

data class Comment(val uid: String, val username: String, val text: String)
