package com.omelchenkoaleks.instagram.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import com.omelchenkoaleks.instagram.R
import com.omelchenkoaleks.instagram.models.User
import com.omelchenkoaleks.instagram.utils.FirebaseHelper
import com.omelchenkoaleks.instagram.utils.GlideApp
import com.omelchenkoaleks.instagram.utils.ValueEventListenerAdapter
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : BaseActivity(4) {
    private val TAG = "ProfileActivity"

    private lateinit var firebase: FirebaseHelper
    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        Log.d(TAG, "onCreate")
        setupBottomNavigation()

        edit_profile_btn.setOnClickListener {
            val intent = Intent(this, EditProfileActivity::class.java)
            startActivity(intent)
        }

        settings_image.setOnClickListener {
            val intent = Intent(this, ProfileSettingsActivity::class.java)
            startActivity(intent)
        }

        add_friends_image.setOnClickListener {
            val intent = Intent(this, AddFriendsActivity::class.java)
            startActivity(intent)
        }

        firebase = FirebaseHelper(this)
        firebase.currentUserReference().addValueEventListener(ValueEventListenerAdapter {
            // получаем юзера
            user = it.asUser()!!
            // загружаем фото
            profile_image.loadUserPhoto(user.photo)
            // загружаем username
            username_text.text = user.username
        })

        /* Recycler имеет виджет, который отвечает за отображение...
                  по сути он состоит из: RecyclerView, LayoutManager, Adapter(ViewHolder) */
        images_recycler.layoutManager = GridLayoutManager(this, 3)

        // получаем наши изображения с базы данных
        firebase.database.child("images").child(firebase.auth.currentUser!!.uid)
            .addValueEventListener(ValueEventListenerAdapter {
                val images = it.children.map { it.getValue(String::class.java) }
                images_recycler.adapter = ImagesAdapter(images as List<String> + images + images + images)
            })
    }
}

// Adapter, который позволяет засовывать в виджет RecyclerView наши данные
class ImagesAdapter(private val images: List<String>) :
    RecyclerView.Adapter<ImagesAdapter.ViewHolder>() {

    /* ViewHolder это патерн, который позовляет кешировать наши, в данном случае ImageView,
    чтобы постоянно не искать их layout и получать на них ссылки */

    class ViewHolder(val image: ImageView) : RecyclerView.ViewHolder(image)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        /* это метод должен просто создавать ViewHolder и в конструктор возвращать View
            (в данном случае ImageView)
            нужно конечно создать layout один для каждой картинки */
        val image = LayoutInflater.from(parent.context)
                .inflate(R.layout.image_item, parent, false) as ImageView
        return ViewHolder(image)
    }

    // метод, в котором нужно в Holder засунуть данные
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.image.loadImage(images[position])
    }

    override fun getItemCount(): Int = images.size
}

class SquareImageView(context: Context, attrs: AttributeSet) : ImageView(context, attrs) {
    // этот метод вызывается, когда layout хочет измерять размер картинки
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        // если мы передадим две ширины - будет картинка квадратная
        super.onMeasure(widthMeasureSpec, widthMeasureSpec)
    }
}
