package com.omelchenkoaleks.instagram.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.omelchenkoaleks.instagram.R
import com.omelchenkoaleks.instagram.models.User
import kotlinx.android.synthetic.main.fragment_register_email.*
import kotlinx.android.synthetic.main.fragment_register_namepass.*

class RegisterActivity : AppCompatActivity(), EmailFragment.Listener, NamePassFragment.Listener {
    private val TAG = "RegisterActivity"

    private var mEmail: String? = null
    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        auth = FirebaseAuth.getInstance()

        // reference = дает возможность получить сразу ссылку на объекты в базе данных
        database = FirebaseDatabase.getInstance().reference

        // на старте активити сразу добавляем фрагмент на экран,
        // но это нужно сделать только при первом создании активити, поэтому проверка на null
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().add(R.id.frame_layout, EmailFragment()).commit()
        }
    }

    // этот реализованый метод интерфейса будет принимать значения полученые во фрагменте в EditText (email)
    override fun onNext(email: String) {
        if (email.isNotEmpty()) {
            // перед навигацией сохраняем значение в поле
            mEmail = email

            // делаем проверку есть ли уже такой email:
            auth.fetchSignInMethodsForEmail(email) { signInMethods ->
                if (signInMethods.isEmpty()) {
                    supportFragmentManager.beginTransaction().replace(R.id.frame_layout, NamePassFragment())
                        .addToBackStack(null)
                        .commit()
                } else {
                    showToast("This email already exists")
                }
            }
        } else {
            showToast("Please enter email")
        }
    }

    // этот метод будет принимать значения полученные во фрагменте в EditText (fullName and password)
    override fun onRegister(fullName: String, password: String) {
        if (fullName.isNotEmpty() && password.isNotEmpty()) {
            // выносим в локальную переменную, чтобы Котлин гарантировал что переменная не null
            val email = mEmail
            if (email != null) {
                auth.createUserWithEmailAndPassword(email, password) {
                    database.createUser(it.user.uid, makeUser(fullName, email)) {
                        startHomeActivity()
                    }
                }
            } else {
                Log.e(TAG, "onRegister: email is null")
                // В этом случае покажем:
                showToast("Please enter email")
                // и вернем пользователя назад
                supportFragmentManager.popBackStack()
            }
        } else {
            showToast("Please enter full name and password")
        }
    }

    private fun unknownRegisterError(it: Task<*>) {
        Log.e(TAG, "Failed to create user profile", it.exception)
        showToast("Something wrong happened. Please try again later")
    }

    private fun startHomeActivity() {
        startActivity(Intent(this, HomeActivity::class.java))
        // чтобы по нажатию кнопки назад пользователь не мог вернуться на страницу регистрации
        finish()
    }

    private fun makeUser(fullName: String, email: String): User {
        val username = makeUsername(fullName)
        return User(name = fullName, username = username, email = email)
    }

    private fun makeUsername(fullName: String) =
        fullName.toLowerCase().replace(" ", ", ")

    private fun FirebaseAuth.fetchSignInMethodsForEmail(
        email: String,
        onSuccess: (List<String>) -> Unit
    ) {
        fetchSignInMethodsForEmail(email).addOnCompleteListener {
            if (it.isSuccessful) {
                onSuccess(it.result!!.signInMethods ?: emptyList<String>())
            } else {
                showToast(it.exception!!.message!!)
            }
        }
    }

    private fun DatabaseReference.createUser(uid: String, user: User, onSuccess: () -> Unit) {
        val reference = child("users").child(uid)
        reference.setValue(user).addOnCompleteListener {
            if (it.isSuccessful) {
                onSuccess()
            } else {
                unknownRegisterError(it)
            }
        }
    }

    private fun FirebaseAuth.createUserWithEmailAndPassword(
        email: String, password: String,
        onSuccess: (AuthResult) -> Unit
    ) {
        createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    onSuccess(it.result!!)
                } else {
                    unknownRegisterError(it)
                }
            }
    }
}


/*-----------------------------------------------  Fragments  -------------------------------------------------------*/

class EmailFragment : Fragment() {
    private lateinit var listener: Listener

    // вот, это тот интерфейс, который поможет нам передать значение email в наше активити,
    // его, конечно же, нужно реализовать в Активити
    interface Listener {
        fun onNext(email: String)
    }

    // инициализация ua фрагмента
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_register_email, container, false)
    }

    // здесь можно получать ссылки на элементы интерфейса
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        coordinateBtnAndInputs(next_btn, email_input)

        next_btn.setOnClickListener {
            // берем по клику на эту кнопку значение
            // (его нужно будет передать в наше Активити),
            // а для этого нужно будет получить ссылку на Активити (onAttach)
            val email = email_input.text.toString()

            // и вот, после создания связи с Активити через Интерфейс и созданное поле,
            // можем использовать эту ссылку вместе с методом:
            listener.onNext(email)

            /*
            В итоге, что происходит в этом методе:
            Когда будет нажата кнопка Next, мы получим значение и передадим его Активити)))
             */
        }
    }

    // вот, в этом методе мы и получим ссылку на Активити (context по сути и есть здесь наше Активити),
    // но, чтобы передать в него данные нашего email нам нужно сделать какой-то интерфейс, который будет
    // иметь метод, принимающий значения нашего email
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        // нужно сделать cast до интерфейса Listener (который мы создали для передачи данных в Активити)
        // и сохраняем полученное значение в поле listener, которое потом можно будет использовать
        // = конечно же, его нужно объявить выше
        listener = context as Listener
    }
}

class NamePassFragment : Fragment() {
    private lateinit var listener: Listener

    interface Listener {
        fun onRegister(fullName: String, password: String)
    }

    // инициализация ua фрагмента
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_register_namepass, container, false)
    }

    // здесь можно получать ссылки на элементы интерфейса
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        coordinateBtnAndInputs(register_btn, full_name_input, password_input)

        register_btn.setOnClickListener {
            val fullName = full_name_input.text.toString()
            val password = password_input.text.toString()
            listener.onRegister(fullName, password)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as Listener
    }
}
