package com.omelchenkoaleks.instagram.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.google.firebase.auth.EmailAuthProvider
import com.omelchenkoaleks.instagram.R
import com.omelchenkoaleks.instagram.models.User
import com.omelchenkoaleks.instagram.utils.CameraHelper
import com.omelchenkoaleks.instagram.utils.FirebaseHelper
import com.omelchenkoaleks.instagram.utils.ValueEventListenerAdapter
import com.omelchenkoaleks.instagram.views.PasswordDialog
import kotlinx.android.synthetic.main.activity_edit_profile.*

class EditProfileActivity : AppCompatActivity(), PasswordDialog.Listener {
    private val TAG = "EditProfileActivity"

    private lateinit var pendingUser: User
    private lateinit var savedUser: User
    private lateinit var firebase: FirebaseHelper
    private lateinit var camera: CameraHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        Log.d(TAG, "onCreate")

        camera = CameraHelper(this)

        back_image.setOnClickListener { finish() }
        save_image.setOnClickListener { updateProfile() }
        change_photo_text.setOnClickListener { camera.takeCameraPicture() }

        firebase = FirebaseHelper(this)

        firebase.currentUserReference()
            .addListenerForSingleValueEvent(ValueEventListenerAdapter {
                savedUser = it.asUser()!!
                name_input.setText(savedUser.name)
                username_input.setText(savedUser.username)
                website_input.setText(savedUser.website)
                bio_input.setText(savedUser.bio)
                email_input.setText(savedUser.email)
                phone_input.setText(savedUser.phone?.toString())
                profile_image.loadUserPhoto(savedUser.photo)
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == camera.REQUST_CODE && resultCode == Activity.RESULT_OK) {
            firebase.uploadUserPhoto(camera.imageUri!!) {
                val photoUrl = it.downloadUrl.toString()
                firebase.updateUserPhoto(photoUrl) {
                    savedUser = savedUser.copy(photo = photoUrl)
                    profile_image.loadUserPhoto(savedUser.photo)
                }
            }
        }
    }

    private fun updateProfile() {
        // для обновления профиля нужно:
        // 1, сначала получить пользователя из inputs,
        // 2, потом сделать валидацию
        pendingUser = readInputs()

        val error = validate(pendingUser)
        if (error == null) {
            if (pendingUser.email == savedUser.email) {
                // update user
                updateUser(pendingUser)
            } else {
                // show dialog
                PasswordDialog().show(supportFragmentManager, "password_dialog")
            }
        } else {
            showToast(error)
        }
    }

    private fun readInputs(): User {
        return User(
            name = name_input.text.toString(),
            username = username_input.text.toString(),
            email = email_input.text.toString(),
            website = website_input.text.toStringOrNull(),
            bio = bio_input.text.toStringOrNull(),
            // теперь, если пользователь не введет номер, будет не ошибка, а просто 0
            phone = phone_input.text.toString().toLongOrNull()
        )
    }

    override fun onPasswordConfirm(password: String) {
        // проверка на пустую строку в диалоговом окне пароля
        if (password.isNotEmpty()) {

            /* -----------------------------------------------------------------------------
                update email in auth, но чтобы это сделать нужно авторизировать user еще раз
                re-authenticate - это метод в Firebase, который помогает это сделать
                ---------------------------------------------------------------------------- */

            val credential = EmailAuthProvider.getCredential(savedUser.email, password)
            firebase.reauthenticate(credential) {
                firebase.updateEmail(pendingUser.email) {
                    updateUser(pendingUser)
                }
            }
        } else {
            showToast("You should enter your password")
        }
    }

    private fun updateUser(user: User) {
        val updateMap = mutableMapOf<String, Any?>()
        if (user.name != savedUser.name) updateMap["name"] = user.name
        if (user.username != savedUser.username) updateMap["username"] = user.username
        if (user.website != savedUser.website) updateMap["website"] = user.website
        if (user.bio != savedUser.bio) updateMap["bio"] = user.bio
        if (user.email != savedUser.email) updateMap["email"] = user.email
        if (user.phone != savedUser.phone) updateMap["phone"] = user.phone

        firebase.updateUser(updateMap) {
            showToast("Profile saved")
            finish()
        }
    }

    // если все правильно будет возвращать null, если не правильно String Error message
    private fun validate(user: User): String? =
        when {
            user.name.isEmpty() -> getString(R.string.enter_name_error)
            user.username.isEmpty() -> getString(R.string.enter_username_error)
            user.email.isEmpty() -> getString(R.string.enter_email_error)
            else -> null
        }
}

