package com.omelchenkoaleks.instagram.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.omelchenkoaleks.instagram.R
import com.omelchenkoaleks.instagram.utils.FirebaseHelper
import kotlinx.android.synthetic.main.activity_profile_settings.*

class ProfileSettingsActivity : AppCompatActivity() {
    private lateinit var firebase: FirebaseHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_settings)

        firebase = FirebaseHelper(this)
        sign_out_text.setOnClickListener { firebase.auth.signOut() }
        back_image.setOnClickListener { finish() }
    }
}
